import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Flutter')),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/about');
              },
              child: const Text('About')),
          const SizedBox(height: 7),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/settings');
              },
              child: const Text('Settings')),
          const SizedBox(height: 7),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/form');
              },
              child: const Text('Form')),
          const SizedBox(height: 7),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/user');
              },
              child: const Text('user')),
          const SizedBox(height: 7),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/forum');
              },
              child: const Text('forum')),
          const SizedBox(height: 7),
          ElevatedButton(
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/message',
                  arguments: 'Message passé en paramètre',
                );
              },
              child: const Text('Message'))
        ]),
      ),
    );
  }
}
