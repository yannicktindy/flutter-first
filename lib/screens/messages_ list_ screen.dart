import 'package:first/model/message.dart';
import 'package:first/model/user.dart';
import 'package:first/controller/forum_controller.dart';
import 'package:flutter/material.dart';

class MessageListScreen extends StatefulWidget {
  const MessageListScreen({Key? key}) : super(key: key);

  @override
  _MessageListScreenState createState() => _MessageListScreenState();
}

class _MessageListScreenState extends State<MessageListScreen> {
  final ForumController _forumController = ForumController();
  final TextEditingController _subjectController = TextEditingController();
  final TextEditingController _contentController = TextEditingController();
  final TextEditingController _authorPrenom = TextEditingController();
  final TextEditingController _authorNom = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Forum'),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: FloatingActionButton.extended(
              onPressed: _showAddMessageDialog,
              backgroundColor: Colors.blue,
              label: const Text('Ajouter message'),
              icon: const Icon(Icons.add),
            ),
          ),
          Expanded(
            child: _buildMessageList(),
          ),
        ],
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
    );
  }

  Widget _buildMessageList() {
    final messages = _forumController.messages;
    if (messages.isEmpty) {
      return const Center(
        child: Text('No messages yet'),
      );
    }

    return ListView.builder(
      itemCount: messages.length,
      itemBuilder: (context, index) {
        final message = messages[index];
        return _buildMessageCard(message);
      },
    );
  }

  Widget _buildMessageCard(Message message) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              message.subject,
              style: const TextStyle(
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(height: 8.0),
            Text(message.content),
            const SizedBox(height: 8.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Icon(Icons.person),
                    Text(
                      '${message.author.prenom} ${message.author.nom}',
                      style: TextStyle(fontSize: 14.0, color: Colors.grey),
                    ),
                  ],
                ),
                Row(
                  children: [
                    Text(
                      '${message.sentDate.day}/${message.sentDate.month}/${message.sentDate.year}',
                      style: TextStyle(fontSize: 14.0, color: Colors.grey),
                    ),
                    IconButton(
                        onPressed: () {
                          onDelete(message);
                        },
                        icon: Icon(Icons.delete))
                  ],
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  void _showAddMessageDialog() {
    showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: const Text('Add Message'),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            TextField(
              controller: _subjectController,
              decoration: const InputDecoration(
                hintText: 'Subject',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _contentController,
              decoration: const InputDecoration(
                hintText: 'Content',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _authorPrenom,
              decoration: const InputDecoration(
                hintText: 'Author Prenom',
              ),
            ),
            const SizedBox(height: 16.0),
            TextField(
              controller: _authorNom,
              decoration: const InputDecoration(
                hintText: 'Author Nom',
              ),
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: const Text('Cancel'),
          ),
          TextButton(
            onPressed: _saveMessage,
            child: const Text('Save'),
          ),
        ],
      ),
    );
  }

  void _saveMessage() {
    final subject = _subjectController.text.trim();
    final content = _contentController.text.trim();
    final authorPrenom = _authorPrenom.text.trim();
    final authorNom = _authorNom.text.trim();

    if (subject.isNotEmpty &&
        content.isNotEmpty &&
        authorPrenom.isNotEmpty &&
        authorNom.isNotEmpty) {
      _forumController.addMessage(
        subject,
        content,
        DateTime.now(),
        User(authorPrenom, authorNom),
      );
      setState(
          () {}); // Appel à setState() pour forcer la mise à jour de l'interface
      Navigator.of(context).pop();
      _subjectController.clear();
      _contentController.clear();
      _authorPrenom.clear();
      _authorNom.clear();
    }
  }
}
