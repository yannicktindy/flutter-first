import 'package:flutter/material.dart';

class AboutScreen extends StatelessWidget {
  const AboutScreen({super.key});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('À propos', style: TextStyle(color: Colors.white)),
      ),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(20.0),
        margin: const EdgeInsets.only(top: 30.0, bottom: 30.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              RichText(
                text: const TextSpan(
                  children: [
                    TextSpan(
                      text: "Mon App",
                      style: TextStyle(
                        color: Colors.blue, // Couleur bleue pour le "D"
                        fontSize: 30.0,
                        fontWeight: FontWeight.bold, // Texte en gras
                      ),
                    ),
                  ],
                ),
              ),
              RichText(
                text: const TextSpan(
                  children: [
                    TextSpan(
                      text: "V.0.0.1",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 10.0,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ],
                ),
              ),
              RichText(
                text: const TextSpan(
                  children: [
                    TextSpan(
                      text: "Développée par : Sam",
                      style: TextStyle(
                        color: Colors.blue,
                        fontSize: 20.0,
                        fontStyle: FontStyle.italic,
                      ),
                    ),
                  ],
                ),
              ),
              Card(
                elevation: 20,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                  side: const BorderSide(color: Colors.blue, width: 2),
                ),
                child: SizedBox(
                  width: 300.0,
                  height: 300.0,
                  child: Image.asset("assets/img/flutter.png"),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
