  import 'package:first/screens/result_screen.dart';
  import 'package:flutter/material.dart';

  class FormScreen extends StatefulWidget {
    const FormScreen({super.key});

    @override
    State<FormScreen> createState() => _FormScreenState();
  }

  class _FormScreenState extends State<FormScreen> {
    final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
    final emailRegex = RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$');

    String? _username;
    String? _email;
    bool? _agreedTerms;
    double _happiness = 0.0;
    String? _gender; // Ajout de la variable pour le genre

    @override
    void initState() {
      super.initState();
      _gender = null; // Initialisation de _gender à null
    }

    @override
    Widget build(BuildContext context) {
      return Scaffold(
        appBar: AppBar(title: const Text('Formulaire')),
        body: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(labelText: 'Pseudo'),
                  onSaved: (value) => _username = value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ce champ est obligatoire';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  decoration: const InputDecoration(labelText: 'Email'),
                  keyboardType: TextInputType.emailAddress,
                  onSaved: (value) => _email = value,
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Ce champ est obligatoire';
                    }
                    if (!emailRegex.hasMatch(value)) {
                      return 'Veuillez entrer une adresse email valide';
                    }
                    return null;
                  },
                ),
                // Ajout du menu déroulant pour le genre
                DropdownButtonFormField<String>(
                  decoration: const InputDecoration(labelText: 'Genre'),
                  value: _gender,
                  onChanged: (value) {
                    setState(() {
                      _gender = value;
                    });
                  },
                  validator: (value) {
                    if (value == null || value.isEmpty) {
                      return 'Veuillez sélectionner un genre';
                    }
                    return null;
                  },
                  items: const [
                    DropdownMenuItem(value: 'homme', child: Text('Homme')),
                    DropdownMenuItem(value: 'femme', child: Text('Femme')),
                  ],
                ),
                Row(
                  children: [
                    Radio(
                      value: true,
                      groupValue: _agreedTerms,
                      onChanged: (value) => setState(() {
                        _agreedTerms = value;
                      }),
                    ),
                    const Text('Accepter les termes'),
                  ],
                ),
                Text("Happiness: ${_happiness.toStringAsFixed(2)}"),
                Slider(
                    value: _happiness,
                    min: 0.0,
                    max: 100,
                    onChanged: (value) {
                      setState(() {
                        _happiness = value;
                      });
                    }),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate() &&
                        _agreedTerms != null) {
                      _formKey.currentState!.save();

                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ResultScreen(
                            username: _username,
                            email: _email,
                            agreedTerms: _agreedTerms,
                            happiness: _happiness,
                            gender: _gender, 
                          ),  
                        ),
                      );
                    }
                  },
                  child: const Text('Envoyer'),
                ),
              ],
            ),
          ),
        ),
      );
    }
  }
