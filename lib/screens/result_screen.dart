import 'package:flutter/material.dart';

class ResultScreen extends StatelessWidget {

  final String? username;
  final String? email;
  final bool? agreedTerms;
  final double? happiness;
  final String? gender;

  const ResultScreen({
    required this.username,
    required this.email, 
    required this.agreedTerms,
    required this.happiness,
    required this.gender
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Résultats')),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Pseudo: $username'),
            Text('Email: $email'), 
            Text('gender: $gender'), // Ajout de l'affichage du genre (
            Text('Accepte terms: $agreedTerms'),
            Text('Happiness: $happiness')  
          ]
        ) 
      )
    );
  }
}