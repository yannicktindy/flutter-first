import 'package:first/model/user.dart';
import 'package:first/controller/user_controller.dart';
// import 'package:first/screens/user_detail_screen.dart';
import 'package:flutter/material.dart';

class UserScreen extends StatefulWidget {
  const UserScreen({super.key});
  @override
  State<StatefulWidget> createState() {
    return _UserScreenState();
  }
}

class _UserScreenState extends State<UserScreen> {
  UserController _userController = UserController();
  String? _firstName;
  String? _lastName;

  @override
  void initState() {
    super.initState();
    _userController.loadUsers();
  }

  Widget _buildUserCard(User user, int index) {
    return Card(
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Row(
          children: [
            Icon(Icons.person, color: index == 0 ? Colors.red : null),
            const SizedBox(width: 16.0),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    user.prenom,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: index == 0 ? Colors.red : null,
                    ),
                  ),
                  const SizedBox(height: 4.0),
                  Text(
                    user.nom,
                    style: TextStyle(
                      fontSize: 14.0,
                      color: index == 0 ? Colors.red : null,
                    ),
                  ),
                ],
              ),
            ),
            IconButton(
              icon: const Icon(Icons.delete, color: Colors.red),
              onPressed: () {
                _userController.users.removeAt(index);
                setState(() {});
              },
            ),
            IconButton(
              icon: const Icon(Icons.remove_red_eye, color: Colors.blue),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/user/detail',
                  arguments: user,
                );
              },
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildUserList() {
    return ListView.builder(
      itemCount: _userController.users.length,
      itemBuilder: (BuildContext context, int index) {
        User user = _userController.users[index];
        return _buildUserCard(user, index);
      },
    );
  }

  void _showUserInputDialog() {
    // Le reste du code de _showUserInputDialog() reste inchangé
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter', style: TextStyle(color: Colors.white)),
        elevation: 10.0,
        centerTitle: true,
      ),
      backgroundColor: const Color.fromRGBO(255, 255, 255, 1),
      body: Column(
        children: [
          InkWell(
            onTap: _showUserInputDialog,
            child: Container(
              width: 200,
              height: 50,
              decoration: BoxDecoration(
                color: Colors.blue,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: const Center(
                child: Text(
                  'Add User',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0,
                  ),
                ),
              ),
            ),
          ),
          const SizedBox(height: 20.0),
          Expanded(
            child: _buildUserList(),
          ),
        ],
      ),
    );
  }
}