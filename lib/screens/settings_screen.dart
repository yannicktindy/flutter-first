import 'package:flutter/material.dart';

class SettingsScreen extends StatefulWidget {
  const SettingsScreen({super.key});

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  bool _isCardSelected = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings', style: TextStyle(color: Colors.white)),
      ),
      body: Container(
        color: Colors.white,
        padding: const EdgeInsets.all(20.0),
        margin: const EdgeInsets.only(top: 30.0, bottom: 30.0),
        child: Center(
          child: InkWell(
            onTap: () {
              setState(() {
                _isCardSelected = !_isCardSelected;
              });
            },
            child: Container(
              width: 200,
              height: 200,
              decoration: BoxDecoration(
                color: _isCardSelected ? Colors.red : Colors.blue,
                borderRadius: BorderRadius.circular(8.0),
              ),
              child: Center(
                child: Text(
                  _isCardSelected ? 'Card is Red' : 'Card is Blue',
                  style: const TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
