import 'package:first/model/user.dart';
import 'package:first/screens/about_screen.dart';
import 'package:first/screens/form_screen.dart';
import 'package:first/screens/home_screen.dart';
import 'package:first/screens/message_screen.dart';
import 'package:first/screens/messages_%20list_%20screen.dart';
import 'package:first/screens/settings_screen.dart';
import 'package:first/screens/user_detail_screen.dart';
import 'package:first/screens/user_screen.dart';
import 'package:flutter/material.dart';


void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter',
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          backgroundColor: Colors.indigo,
        ),
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      routes: {
        '/': (context) => const HomeScreen(),
        '/about': (context) => const AboutScreen(),
        '/settings': (context) => const SettingsScreen(),
        '/message': (context) => const MessageScreen(),
        '/form': (context) => const FormScreen(),
        '/user': (context) => const UserScreen(),
        '/user/detail': (context) => UserDetailScreen(user: ModalRoute.of(context)!.settings.arguments as User),
        '/forum': (context) => const MessageListScreen(),
      },
    );
  }
}

