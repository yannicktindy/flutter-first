import 'package:first/model/message.dart';
import 'package:first/model/user.dart';
// import 'package:flutter/material.dart';

class ForumController {
  final List<Message> _messages = [];

  List<Message> get messages => _messages;

  void addMessage(String subject, String content, DateTime sentDate, User author) {
    _messages.add(
      Message(
        subject: subject,
        content: content,
        sentDate: sentDate,
        author: author,
      ),
    );
  }

  void deleteMessage(Message message) {
    _messages.remove(message); 
  }
}