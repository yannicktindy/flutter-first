import 'package:first/model/user.dart';

class Message {
  String _subject;
  String _content;
  DateTime _sentDate;
  User _author;

  Message({
    required String subject,
    required String content,
    required DateTime sentDate,
    required User author,
  })  : _subject = subject,
        _content = content,
        _sentDate = sentDate,
        _author = author;

  // Getters
  String get subject => _subject;
  String get content => _content;
  DateTime get sentDate => _sentDate;
  User get author => _author;

  // Setters
  set subject(String value) {
    _subject = value;
  }

  set content(String value) {
    _content = value;
  }

  set sentDate(DateTime value) {
    _sentDate = value;
  }

  set author(User value) {
    _author = value;
  }
}